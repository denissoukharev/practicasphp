<?php

// opcion 1
// no es valido para el interior de clases
define ("NOMBRE","DENIS");

// opcion 2
// es valido siempre
const NOMBRE = "DENIS";
