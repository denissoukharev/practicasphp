<?php

function generaColores($numero,$almohadilla=true){
    $colores=array();
    for($n=0;$n<$numero;$n++){
        $c=0;
        $limite=6;
        $colores[$n]="";
        if($almohadilla){
            $colores[$n]="#";
            $limite=7;
        }
        for(;$c<$limite;$c++){
            $colores[$n].=dechex(mt_rand(0,15));
        }
    }
    return $colores;
}

var_dump(generaColores(7,true));

