<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 2</title>
    </head>
    <body>
        <h1>Ejercicicio 2 de la practica 1</h1>
        <table width="100%" border="1">
            <tr>
                <td>
                    <?php 
                    //Podemos utilizar o comillas simples comillas doble para el texto
                    echo "Este texto esta escrito utilizando la funcion echo de PHP";
                    ?>
                </td>
                <td>Este texto esta escrito en HTML</td>
            </tr>
            <tr>
                <td>
                    <?php
                    print 'Este texto esta escrito desde PHP con la funcion print';
                    ?>
                </td>
                <td>
                    <?="Centro de formacion Alpe" ?>
                </td>
            </tr>
        </table>
        
    </body>
</html>
