<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
       <form action="ejercicio4Destino.php" >
            <div>
                <label for="nombre">
                    Nombre:
                </label>
                <input type="text" name="nombre" id="nombre" placeholder="Introduce tu nombre">
            </div>
            <br>
            <div>
                <label for="email">
                    Email:
                </label>
                <input type="email" id="correo" name="email"  placeholder="Introduce tu correo">
            </div>
            <div>
                <button formmethod="GET">Enviar por GET</button>
                <button formmethod="POST">Enviar por POST</button>
            </div>
    </body>
</html>