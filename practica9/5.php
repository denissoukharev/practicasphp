<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form action="ejercicio5Destino.php" method="post">
            <div>
                <label for="nombre">
                Nombre
                </label>
                <input type="text" name="nombre" id="nombre">
            </div>
            <div>
                <label for="apellido">
                Apellidos
                </label>
            <input type="text" name="apellido" id="apellido">
            </div>
            <div>
                <label for="codigo">
                Codigo Postal
                </label>
                <input type="numero" name="codigo" id="codigo">
            </div>
            <div>
                <label for="telefono">
                Telefono
                </label>
                <input type="numero" name="telefono" id="telefono">
            </div>
            <div>
                <label for="correo">
                Correo
                </label>
                <input type="email" name="correo" id="correo">
            </div>
            <div>
                <button>Enviar</button>
            </div>
        </form>
    </body>
</html>
