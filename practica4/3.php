<?php

// Creando variables
$nombre="Ramon";
$poblacion="Santander";


// solucion 1
echo "div";
echo $nombre;
echo "</div>";
echo "div";
echo $poblacion;
echo "</div>";


// solucion 2
echo "<div>" . $nombre . "</div>";
echo "<div>" . $poblacion . "</div>";

//solucion 3
echo "<div> $nombre </div>";
echo "<div> $poblacion </div>";

// solucion 4
echo "<div> {$nombre} </div>";
echo "<div> {$poblacion} </div>";

// solucion 5 utilizando heredoc
$salida=<<<"BLOQUE"
<div> {$nombre} </div>
<div> {$poblacion} </div>
BLOQUE;

echo $salida;

// solucion 6 utilizando heredoc 
$salida=<<<BLOQUE
<div> {$nombre} </div>
<div> {$poblacion} </div>
BLOQUE;

echo $salida;




?>






