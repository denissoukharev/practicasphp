
<?php

function vocales(string $palabra) {
    $counter = 0;
    $voc = ["a", "e", "i", "o", "u"];
    for ($a = 0; $a < strlen($palabra); $a++) {
        for ($n = 0; $n < count($voc); $n++) {

            if ($palabra[$a] == $voc[$n]) {
                $counter++;
                break;
            }
        }
    }
    echo $counter;
}
?>

<!DOCTYPE html>
<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHPWebPage.php to edit this template
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        vocales('alpe');
        ?>
    </body>
</html>
